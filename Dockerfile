#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto a exponer
EXPOSE 3000

#Comando para ejecutar
CMD ["npm", "start"]

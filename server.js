//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req, res, next){
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

  var requestjson = require('request-json');
    var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bdpractitionerteam1/collections/movimientos?apiKey=OyHVw7izv6mqlruHdcv-gJ0t6_I6vqHz";
    var urlusuariosMLab = "https://api.mlab.com/api/1/databases/bdpractitionerteam1/collections/usuarios?apiKey=OyHVw7izv6mqlruHdcv-gJ0t6_I6vqHz";
    var clienteMLab = requestjson.createClient(urlmovimientosMLab);
    var clienteMLabU = requestjson.createClient(urlusuariosMLab);
    var urlMlabBase="https://api.mlab.com/api/1/databases/bdpractitionerteam1/collections/movimientos/"
    var MlabKey="?apiKey=OyHVw7izv6mqlruHdcv-gJ0t6_I6vqHz"



var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
app.get("/",function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get("/clientes/:idcliente",function(req, res){
  res.send('Aqui tiene al cliente numero : '+ req.params.idcliente);
});

//coleccion movimientos
app.get("/buscaId/:id", function(req,res){
  var idcliente = req.params.id
  var query = '&q={"idcliente":'+idcliente+'}'
  var buscaCliente = urlmovimientosMLab+query
  var clienteMlabFindID=requestjson.createClient(buscaCliente);
  clienteMlabFindID.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
});
//coleccion usuarios
app.get("/buscaIdUsuario/:id", function(req,res){
  var idcliente = req.params.id
  var query = '&q={"idcliente":'+idcliente+'}'
  var buscaCliente = urlusuariosMLab+query
  var clienteMlabFindIDU=requestjson.createClient(buscaCliente);
  clienteMlabFindIDU.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
});


//app.put("/actualizaId/:idcliente",function(req, res){
  //res.send('Aqui tiene al cliente numero : '+ req.params.idcliente);
//});

//Actualiza

app.put("/actualiza/:id", function(req,res){
  var idcliente = req.params.id
  var query = '&q={"idcliente":'+idcliente+'}'
  var actualizaCliente = urlmovimientosMLab+query
  var clienteMlabAct=requestjson.createClient(actualizaCliente);
  var body = JSON.parse('{"$set":'+JSON.stringify(req.body)+'}')
  clienteMlabAct.put('', body, function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
    })
    });

//coleccion usuarios
    app.put("/actualizaUsuario/:id", function(req,res){
      var idcliente = req.params.id
      var query = '&q={"idcliente":'+idcliente+'}'
      var actualizaCliente = urlusuariosMLab+query
      var clienteMlabActU=requestjson.createClient(actualizaCliente);
      var body = JSON.parse('{"$set":'+JSON.stringify(req.body)+'}')
      clienteMlabActU.put('', body, function(err, resM, body){
        if(err){
          console.log(body);
        }else{
          res.send(body);
        }
        })
        });


app.post("/", function(req, res){
  res.send('Hemos recibido su peticion de post cambiada');
});
app.put("/", function(req, res){
  res.send('Hemos recibido su peticion de put');
});
app.delete("/", function(req, res){
  res.send('Hemos recibido su peticion de delete');
});
app.get("/movimientos",function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
});
});
app.post("/movimientos", function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body ){
    res.send(body);
  });
});

app.post("/usuarios", function(req, res){
  clienteMLabU.post('', req.body, function(err, resM, body ){
    res.send(body);
  });
});

//Alta Cliente
app.post("/altacliente",function(req, res){
  var clienteMlabLoad= requestjson.createClient(urlmovimientosMlab)
  clienteMlabLoad.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})
//busca para el borrado
app.get("/busca/:id", function(req,res){
  var idcliente = req.params.id
  //var query = '&q={"idCustomer":'+idcliente+'}&f={"_id":1}'
  var query = '&q={"idcliente":'+idcliente+'}'
  var buscaCliente=urlmovimientosMLab+query
  var clienteMlabFind=requestjson.createClient(buscaCliente);
  clienteMlabFind.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
});
//elimina cliente
app.delete("/delete/:id", function(req, res){
   var id = req.params.id
   var urltotal= urlMlabBase+id+MlabKey
   var clienteMlabDel = requestjson.createClient(urltotal)
   clienteMlabDel.delete('', function(err, resM, body) {
     res.send();
   })
   })
